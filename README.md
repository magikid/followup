# FollowUp
## The Simple Command Chaining Script for iTerm2

![Demo GIF](https://i.imgur.com/yNy4bVZ.gif)

### Dependencies
* python3
* pip3 install iterm2
    * Should be covered by the runtime, but just to be safe.
* iterm2 version 3.3 beta or higher.
    * If you aren't running the 3.3 beta you can join by going to iterm2 > preferences > check for beta updates
    * Install iterm2 shell integrations
        * iterm2 > install shell integrations
            * Be sure to restart your iterm2 after doing so.
    * Install the iterm2 python runtime via the iterm2 > scripts > manage > install python runtime

### Quick Start Guide
* Run `make install`
* Setup your shortcut or touchbar button to trigger the script `FollowUpCommandRunner.py`
    * The script will be located under `Scripts > FollowUp > FollowUpCommandRunner.py` in the select `Select Menu Item` option for a touchbar key or hotkey configuration of iTerm2.
* Optionally if you wish to use the `FollowUpCommandDisplay` script, which shows you what follow up commands are pending, you can repeat the same step you did for the `FollowUpCommandRunner` and create a shortcut for running the `FollowUpCommandDisplay` script.
