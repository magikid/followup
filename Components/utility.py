# Utility functions common to both FollowUp scripts.

from typing import Dict, List
import iterm2

__private_key = 'user.liJkbFollowupCommands'

def get_current_session(app: iterm2.App) -> iterm2.Session: 
    return app.current_terminal_window.current_tab.current_session


def convert_command_list_to_str(commands: List[str]) -> str:
    stringified_commands: str = ' '.join(commands)
    stringified_commands.rstrip(" \n")
    stringified_commands += '\n'
    return stringified_commands


async def async_get_follow_up_variable(connection: iterm2.connection.Connection) -> Dict[str, List[str]]:
    global __private_key
    app: iterm2.App = await iterm2.async_get_app(connection)
    session: iterm2.Session = app.current_terminal_window.current_tab.current_session
    follow_up_commands = await session.async_get_variable(__private_key)

    return follow_up_commands if follow_up_commands else []


async def async_update_follow_up_variable(connection: iterm2.connection.Connection, value: List[str]):
    global __private_key
    app: iterm2.App = await iterm2.async_get_app(connection)
    session: iterm2.Session = app.current_terminal_window.current_tab.current_session
    await session.async_set_variable(__private_key, value)
