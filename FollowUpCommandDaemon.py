#!/usr/bin/env python3.7

import asyncio
import iterm2
from typing import List
from Components import async_get_follow_up_variable, async_update_follow_up_variable, convert_command_list_to_str


async def async_execute_pending_commands_for(connection: iterm2.connection.Connection, session_id: str):
    commands_to_run: List[str] = await async_get_follow_up_variable(connection)
    if not commands_to_run:
        print("No pending follow up commands.")
        return
    commands = convert_command_list_to_str(commands_to_run)
    app: iterm2.App = await iterm2.async_get_app(connection)
    session: iterm2.Session = app.get_session_by_id(session_id)
    await session.async_send_text(commands)
    commands_to_run.clear()
    await async_update_follow_up_variable(connection, commands_to_run)


async def monitor_session_for_follow_up_commands(connection: iterm2.connection.Connection, session_id: str):
    async with iterm2.PromptMonitor(connection, session_id) as mon:
        while True:
            await mon.async_get()
            await async_execute_pending_commands_for(connection, session_id)


async def main(connection):
    app = await iterm2.async_get_app(connection)
    for window in app.terminal_windows:
        for tab in window.tabs:
            for session in tab.sessions:
                asyncio.create_task(monitor_session_for_follow_up_commands(connection, session.session_id))

    async with iterm2.NewSessionMonitor(connection) as mon:
        while True:
            session_id = await mon.async_get()
            asyncio.create_task(monitor_session_for_follow_up_commands(connection, session_id))


# This instructs the script to run the "main" coroutine and to keep running even after it returns.
iterm2.run_forever(main)
